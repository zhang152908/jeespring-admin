<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />

<section class="content-header">
	<h1>
		用户管理<small>用户列表</small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="${ctx}/index.jsp"><i class="fa fa-dashboard"></i>首页</a>
		</li>
		<li>
			<a href="#">用户管理</a>
		</li>
		<li class="active">全部用户</li>
	</ol>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">用户列表</h3>
		</div>
		<div class="box-body">
			<div class="table-box">
				<div class="pull-left">
					<div class="form-group form-inline">
						<div class="btn-group">
							<button type="button" class="btn btn-default" title="新建" onclick="load('${ctx}/user/add.do')">
								<i class="fa fa-file-o"></i> 新建
							</button>
							<button type="button" class="btn btn-default" title="刷新">
								<i class="fa fa-refresh"></i> 刷新
							</button>
						</div>
					</div>
				</div>
				<div class="box-tools pull-right">
					<div class="has-feedback">
						<input type="text" class="form-control input-sm" placeholder="搜索"><span
							class="glyphicon glyphicon-search form-control-feedback"></span>
					</div>
				</div>
				<table id="dataList" class="table table-bordered table-striped table-hover dataTable">
					<thead>
						<tr>
							<th class="" style="padding-right: 0px">
								<input id="selectAll" type="checkbox" class="icheckbox_square-blue">
							</th>
							<th class="sorting_asc">ID</th>
							<th class="sorting_desc">用户名</th>
							<th class="sorting_asc sorting_asc_disabled">邮箱</th>
							<th class="sorting_desc sorting_desc_disabled">联系电话</th>
							<th class="sorting">状态</th>
							<th class="text-center">操作</th>
						</tr>
					</thead>
					<tbody>

					<c:forEach items="${userList}" var="user">
						<tr>
							<td><input name="ids" type="checkbox"></td>
							<td>${user.id}</td>
							<td>${user.username}</td>
							<td>${user.email}</td>
							<td>${user.phone}</td>
							<td>${user.status}</td>
							<td class="text-center">
								<a href="${ctx}/user/findById.do?id=${user.id}" class="btn bg-olive btn-xs">详情</a>
								<a href="${ctx}/user/findUserByIdAndAllRole.do?id=${user.id}" class="btn bg-olive btn-xs">添加角色</a>
							</td>
						</tr>
					</c:forEach>
					</tbody>

				</table>
			</div>
		</div>
		<div class="box-footer">
			<div class="pull-left">
				<div class="form-group form-inline">
					总共2 页，共19条数据。 每页 <select class="form-control">
					<option>1</option>
					<option>2</option>
					<option>3</option>
					<option>4</option>
					<option>5</option>
				</select> 条
				</div>
			</div>
			<div class="box-tools pull-right">
				<ul class="pagination">
					<li><a href="#" aria-label="Previous">首页</a></li>
					<li><a href="#">上一页</a></li>
					<li><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#">下一页</a></li>
					<li><a href="#" aria-label="Next">尾页</a></li>
				</ul>
			</div>
		</div>
	</div>
</section>
<script>
    $(document).ready(function() {
        // 列表按钮
        $("#dataList td input[type='checkbox']").iCheck(
            {
                checkboxClass : 'icheckbox_square-blue',
                increaseArea : '20%'
            });
        // 全选操作
        $("#selectAll").click(function() {
            var clicks = $(this).is(':checked');
            if (!clicks) {
                $("#dataList td input[type='checkbox']").iCheck("uncheck");
            } else {
                $("#dataList td input[type='checkbox']").iCheck("check");
            }
            $(this).data("clicks",!clicks);
        });
    });
</script>