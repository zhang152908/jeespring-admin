<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="${ctx}/img/user2-160x160.jpg" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p>前尘忆梦</p>
				<a href="#"><i class="fa fa-circle text-success"></i>在线</a>
			</div>
		</div>
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header">菜单</li>
			<li class="treeview">
				<a href="${ctx}" data-url="/main.do">
					<i class="fa fa-dashboard"></i><span>主页</span>
				</a>
			</li>

			<li class="treeview">
				<a href="#"><i class="fa fa-cogs"></i>
					<span>系统管理</span>
						<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li>
						<a href="#" data-url="${ctx}/user/list.do"><i class="fa fa-circle-o"></i>用户管理</a>
					</li>
					<li>
						<a href="#" data-url="${ctx}/role/list.do"><i class="fa fa-circle-o"></i>角色管理</a>
					</li>
					<li>
						<a href="#" data-url="${ctx}/permission/list.do"><i class="fa fa-circle-o"></i>资源权限管理</a>
					</li>
					<li>
						<a href="#" data-url="${ctx}/sysLog/list.do"><i class="fa fa-circle-o"></i>访问日志</a>
					</li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#"><i class="fa fa-cube"></i><span>基础数据</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
				<ul class="treeview-menu">
					<li>
						<a href="#" data-url="${ctx}/product/findAll.do" ><i class="fa fa-circle-o"></i>产品管理</a>
					</li>
					<li>
						<a href="#" data-url="${ctx}/orders/findAll.do" ><i class="fa fa-circle-o"></i>订单管理</a>
					</li>
				</ul>
			</li>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>